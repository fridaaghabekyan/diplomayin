<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Online Examination System</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="css/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.4.0.min.js"
        integrity="sha256-BJeo0qm959uMBGb65z40ejJYGSgR7REI4+CW1fNKwOg=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
        integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">


    <script src="js/main.js"></script>

</head>


<body class="bg-black">
    <div class="form-box" id="login-box">
        <div class="header">Registeration</div>
        <form method="POST" id="idForm">
            <div class="body bg-gray">
                <section class="content">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6 text-center">
                                <label for="teacher">
                                    <input type="radio" id="teacher" style="margin-right:0px !important" name="role"
                                        value="teacher"> Teacher
                                </label>
                            </div>
                            <div class="col-md-6 text-center">
                                <label for="student">
                                    <input type="radio" id="student" style="margin-right:0px !important" name="role"
                                        value="student">
                                    Student
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group has-error">
                        <input type="text" name="username" class="form-control username" required placeholder="Username">
                    </div>
                    <div class="form-group">
                        <input type="text" name="email" class="form-control" placeholder="Email address">
                    </div>
                    <div class="form-group has-error">
                        <input type="password" name="password" class="form-control " required placeholder="Password">
                    </div>
                    <!-- <div class="form-group">
                        <select class="form-control" name="class">
                            <option value="1">1</option>
                            <option value="1">2</option>
                            <option value="1">3</option>
                            <option value="1">4</option>
                        </select>
                    </div> -->

                    <div class="form-group">
                        <input type="date" name="birthday" class="form-control datemask" placeholder="Birthday">
                    </div>
                    <div date-picker="" selector=".datemask"></div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-4">
                                <label for="male">
                                    <input type="radio" id="male" name="gender" value="male" class=""> Male
                                </label>
                            </div>
                            <div class="col-md-4">
                                <label for="female">
                                    <input type="radio" id="female" name="gender" value="female" class=""> Female
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="text" name="address" class="form-control" placeholder="Address">
                    </div>
                    <div class="form-group">
                        <input type="text" name="phoneNo" class="form-control" placeholder="Phone No">
                    </div>
                </section>
            </div>
            <div class="footer">
                <section class="content">
                    <button type="submit" class="btn bg-olive btn-block">Register a new membership</button>
                </section>
            </div>
        </form>
        <br><br><br>
    </div>
</body>
<script>
$("#idForm").submit(function(e) {
    e.preventDefault();
    $.ajax({
        type: "POST",
        url: 'sign.php',
        data: {
            username: $("input[name=username]").val(),
            role: $("input[name=role]:checked").val(),
            // class: $("input[name=class]").val(),
            password: $("input[name=password]").val(),
            email: $("input[name=email]").val(),
            gender: $("input[name=gender]:checked").val(),
            birthday: $("input[name=birthday]").val(),
            address: $("input[name=address]").val(),
            phoneNo: $("input[name=phoneNo]").val()
        },
        success: function(output) {
            setTimeout(function() {
                document.location.href = "dashboard.php"
            }, 500);
        },
        error: function(request, status, error) {
            alert(request + " " + status + " " + error);
        }
    });
});
</script>

</html>