
                <div class="box-header">
                    <h3 class="box-title ">Add exam</h3>
                    <a href="dashboard.php?q=0"
                        class="floatRTL btn btn-danger btn-flat pull-right marginBottom15 ng-binding">Cancel add</a>
                </div>
                <div class="box-body table-responsive">
                    <form class="form-horizontal" method="post" name="addExam" role="form" id="idForm">
                        <div class="form-group form-row">
                            <label for="inputEmail3" class="col-sm-2 control-label ">Exam Name * </label>
                            <div class="col-sm-10">
                                <input type="text" name="examTitle" class="form-control " required=""
                                    placeholder="Exam Name">
                            </div>
                        </div>
                        <div class="form-group form-row has-error">
                            <label for="inputPassword3" class="col-sm-2 control-label ">Exam question count
                                *</label>
                            <div class="col-sm-10">
                                <input type="text" id="datemask" name="examTime" required=""
                                    class="form-control datemask ">
                            </div>
                        </div>
                        
                        <div class="form-group form-row">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-default">Add exam</button>
                            </div>
                        </div>
                    </form>
                </div>
            </main>

        </div>
    </div>

</body>

<script>
$("#idForm").submit(function(e) {

    e.preventDefault();

    $.ajax({
        type: "POST",
        url: 'update.php',
        data: {
            examTitle: $("input[name=examTitle]").val(),
            examTime: $("input[name=examTime]").val(),
            countofquestion: $("input[name=examTime]").val()*3,
        },
        success: function(output) {
            alert(output)
            setTimeout(function() {
                document.location.href = "dashboard.php?q=4"
            }, 500);
        },
        error: function(request, status, error) {
            alert(request + " " + status + " " + error);
        } 
    });


});
</script>

</html>