<div class="box-header">
    <h3 class="box-title ">Add exam</h3>
    <a href="dashboard.php?q=0" class="floatRTL btn btn-danger btn-flat pull-right marginBottom15 ng-binding">Cancel
        add</a>
</div>
<div class="box-body table-responsive container-fluid">
    <?php 
        include_once 'dbConnection.php';

        $countofquestion = @$_GET['countofquestion']; 
        $count = $countofquestion/3; 
        $id = @$_GET['id']; 
        for( $i = 0; $i < $countofquestion+1; $i++ ) {
            echo '
            <form class="form-horizontal" method="post" name="addExam" role="form" id="idForm"
            action="operations.php?q=addqns&n='.$countofquestion.'&examID='.$id.'">
            ';
            if($i<=$count-1) {
                echo '<h3>Easy Question</h3>';
                echo'
                <input type="text" value="'.$countofquestion.'" name="questioncount" style="display:none">
                <b>Question number&nbsp;'.$i.'&nbsp;:</b>
                    <div class="form-group">
                        <label class="col-md-12 control-label" for="qns'.$i.' "></label>
                        <div class="col-md-12">
                            <textarea rows="3" cols="5" name="qns'.$i.'" required class="form-control"
                                placeholder="Write question number '.$i.' here..."></textarea>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-12 control-label" for="'.$i.'1"></label>
                        <div class="col-md-12">
                            <input id="'.$i.'1" name="'.$i.'1" placeholder="Enter option a" required class="form-control input-md"
                                type="text">

                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-12 control-label" for="'.$i.'2"></label>
                        <div class="col-md-12">
                            <input id="'.$i.'2" name="'.$i.'2" placeholder="Enter option b" required class="form-control input-md"
                                type="text">

                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-12 control-label" for="'.$i.'3"></label>
                        <div class="col-md-12">
                            <input id="'.$i.'3" name="'.$i.'3" placeholder="Enter option c" required class="form-control input-md"
                                type="text">

                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-12 control-label" for="'.$i.'4"></label>
                        <div class="col-md-12">
                            <input id="'.$i.'4" name="'.$i.'4" placeholder="Enter option d" required class="form-control input-md"
                                type="text">

                        </div>
                    </div>
                    
                    <b>Correct answer</b>:
                    <select id="ans'.$i.'" name="ans'.$i.'" placeholder="Choose correct answer " required class="form-control input-md">
                        <option value="a">Select answer for question '.$i.'</option>
                        <option value="a">option a</option>
                        <option value="b">option b</option>
                        <option value="c">option c</option>
                        <option value="d">option d</option>
                    </select>
                    <div class="form-group form-row" style="display:none" >
                        <label for="inputEmail3" class="col-sm-2 control-label ">Bardutyan astichan</label>
                        <div class="col-sm-10">
                            <div class="custom-control custom-radio">
                                <input type="radio" required class="custom-control-input" checked id="easy'.$i.'" value="easy"
                                    name="hardly'.$i.'">
                                <label class="custom-control-label" for="easy'.$i.'">Easy</label>
                            </div>
                            
                        </div>
                    </div>';
            } 

            if ($i>$count-1 and $i<$count*2){
                echo '<h3>Medium Question</h3>';
                echo'
                <input type="text" value="'.$countofquestion.'" name="questioncount" style="display:none">
                <b>Question number&nbsp;'.$i.'&nbsp;:</b>
                    <div class="form-group">
                        <label class="col-md-12 control-label" for="qns'.$i.' "></label>
                        <div class="col-md-12">
                            <textarea rows="3" cols="5" name="qns'.$i.'" required class="form-control"
                                placeholder="Write question number '.$i.' here..."></textarea>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-12 control-label" for="'.$i.'1"></label>
                        <div class="col-md-12">
                            <input id="'.$i.'1" name="'.$i.'1" placeholder="Enter option a" required class="form-control input-md"
                                type="text">

                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-12 control-label" for="'.$i.'2"></label>
                        <div class="col-md-12">
                            <input id="'.$i.'2" name="'.$i.'2" placeholder="Enter option b" required class="form-control input-md"
                                type="text">

                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-12 control-label" for="'.$i.'3"></label>
                        <div class="col-md-12">
                            <input id="'.$i.'3" name="'.$i.'3" placeholder="Enter option c" required class="form-control input-md"
                                type="text">

                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-12 control-label" for="'.$i.'4"></label>
                        <div class="col-md-12">
                            <input id="'.$i.'4" name="'.$i.'4" placeholder="Enter option d" required class="form-control input-md"
                                type="text">

                        </div>
                    </div>
                    
                    <b>Correct answer</b>:
                    <select id="ans'.$i.'" name="ans'.$i.'" placeholder="Choose correct answer " required class="form-control input-md">
                        <option value="a">Select answer for question '.$i.'</option>
                        <option value="a">option a</option>
                        <option value="b">option b</option>
                        <option value="c">option c</option>
                        <option value="d">option d</option>
                    </select>
                    <div class="form-group form-row"  style="display:none">
                        <label for="inputEmail3" class="col-sm-2 control-label ">Bardutyan astichan</label>
                        <div class="col-sm-10">
                            <div class="custom-control custom-radio">
                                <input type="radio" required class="custom-control-input" checked id="medium'.$i.'" value="medium"
                                    name="hardly'.$i.'">
                                <label class="custom-control-label" for="medium'.$i.'">Medium</label>
                            </div>
                        </div>
                    </div>';
            }

            if($i>$count*2 and $i<=$countofquestion+1){
                echo '<h3>Hard Question</h3>';
                echo'
                <input type="text" value="'.$countofquestion.'" name="questioncount" style="display:none">
                <b>Question number&nbsp;'.$i.'&nbsp;:</b>
                    <div class="form-group">
                        <label class="col-md-12 control-label" for="qns'.$i.' "></label>
                        <div class="col-md-12">
                            <textarea rows="3" cols="5" name="qns'.$i.'" required class="form-control"
                                placeholder="Write question number '.$i.' here..."></textarea>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-12 control-label" for="'.$i.'1"></label>
                        <div class="col-md-12">
                            <input id="'.$i.'1" name="'.$i.'1" placeholder="Enter option a" required class="form-control input-md"
                                type="text">

                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-12 control-label" for="'.$i.'2"></label>
                        <div class="col-md-12">
                            <input id="'.$i.'2" name="'.$i.'2" placeholder="Enter option b" required class="form-control input-md"
                                type="text">

                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-12 control-label" for="'.$i.'3"></label>
                        <div class="col-md-12">
                            <input id="'.$i.'3" name="'.$i.'3" placeholder="Enter option c" required class="form-control input-md"
                                type="text">

                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-12 control-label" for="'.$i.'4"></label>
                        <div class="col-md-12">
                            <input id="'.$i.'4" name="'.$i.'4" placeholder="Enter option d" required class="form-control input-md"
                                type="text">

                        </div>
                    </div>
                    
                    <b>Correct answer</b>:
                    <select id="ans'.$i.'" name="ans'.$i.'" placeholder="Choose correct answer " required class="form-control input-md">
                        <option value="a">Select answer for question '.$i.'</option>
                        <option value="a">option a</option>
                        <option value="b">option b</option>
                        <option value="c">option c</option>
                        <option value="d">option d</option>
                    </select>
                    <div class="form-group form-row"  style="display:none">
                        <label for="inputEmail3" class="col-sm-2 control-label ">Bardutyan astichan</label>
                        <div class="col-sm-10">
                            <div class="custom-control custom-radio">
                                <input type="radio" required class="custom-control-input" checked id="hard'.$i.'" value="hard"
                                    name="hardly'.$i.'">
                                <label class="custom-control-label" for="hard'.$i.'">Hard</label>
                            </div>
                        </div>
                    </div>';
            }
            
        }
    ?>
        <div class="form-group form-row">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default submit">Add questions</button>
            </div>
        </div>
    </form>
</div>


<script>

$('.submit').click(function(){
    $("#idForm").reset(); 
})
// $("#idForm").submit(function(e) {

// e.preventDefault();

// var countofquestion = $("input[name=questioncount]").val();

// console.log(countofquestion);
// for (let i = 0; i < countofquestion; i++) {
//     var question = $("textarea[name=qns" + i + "]").val();
//     var ans1 = $("input[name=" + i + "1]").val();
//     var ans2 = $("input[name=" + i + "2]").val();
//     var ans3 = $("input[name=" + i + "3]").val();
//     var ans4 = $("input[name=" + i + "4]").val();
//     var selectedCountry = $("select#ans" + i).val();
//     var hardly = $("input[name=hardly" + i + "]:checked").val();
//     console.log(countofquestion + " " + question + " " + ans1 + " " + ans2 + " " + ans3 + " " + ans4 + " " +
//         selectedCountry + " " + hardly);


//     $.ajax({
//         type: "POST",
//         url: 'operations.php',
//         data: {
//             question: question,
//             countofquestion: countofquestion,
//             ans1: ans1,
//             ans2: ans2,
//             ans3: ans3,
//             ans4: ans4,
//             selectedCountry: selectedCountry,
//             hardly: hardly,
//         },
//         success: function(output) {
//             alert(output)
//             setTimeout(function() {
//                 document.location.href = "dashboard.php?q=11&countofquestion=" +
//                     countofquestion
//             }, 500);
//         },
//         error: function(request, status, error) {
//             alert(request + " " + status + " " + error);
//         }
//     });
// }

// });
</script>