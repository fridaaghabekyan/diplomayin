<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Online Examination System</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="css/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.4.0.min.js"
        integrity="sha256-BJeo0qm959uMBGb65z40ejJYGSgR7REI4+CW1fNKwOg=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
        integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">

    <script src="js/main.js"></script>

</head>

<body class="bg-black">
    <div class="form-box" id="login-box">
        <div class="header">Sign in</div>
        <form method="post" id="idForm">
            <div class="body bg-gray">
                <div class="form-group">
                    <input type="text" name="username" class="form-control" required placeholder="Username">
                </div>
                <div class="form-group">
                    <input type="password" name="password" class="form-control" required placeholder="Password">
                </div>
                <div class="form-group">
                    <label for="rmbr">
                        <input type="checkbox" name="remember_me" value="" id="rmbr">Remember me
                    </label>
                </div>
            </div>
            <div class="footer">
                <button type="submit" class="btn bg-olive btn-block">Sign in</button>
                <p><a href="restorepassword.php">Restore Password</a></p>
                <a href="register.php" class="text-center">Register a new membership</a>
            </div>
        </form>
    </div>

</body>

<script>
$("#idForm").submit(function(e) {
    e.preventDefault();
    $.ajax({
        type: "POST",
        url: 'login.php',
        data: {
            password: $("input[name=password]").val(),
            username: $("input[name=username]").val()
        },
        success: function(output) {
            setTimeout(function() {
                document.location.href = "dashboard.php?q=0"
            }, 500);
        },
        error: function(request, status, error) {
            alert(request + " " + status + " " + error);
        }
    });


});
</script>

</html>