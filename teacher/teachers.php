    <h3>Teachers</h3>
    <br>

    <section class="content ">
        <?php
            if($name == 'admin'){
                echo '<a href="dashboard.php?q=7" class="floatRTL btn btn-success btn-flat pull-right marginBottom15 addExam">Add
                teacher</a>';
            }
        ?>

        <br>
        <hr>
        <div class="box col-xs-12">
            <div class="box-header">
                <h3 class="box-title">List teachers
                </h3>

            </div>
            <div class="box-body table-responsive">
                <table class="table table-hover">
                    <tbody>
                        <tr>
                            <th>ID</th>
                            <th>Fullname</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Address</th>
                            <?php
                                if($name == 'admin'){
                                    echo '<th>Operations</th>';
                                }
                            ?>
                        </tr>
                        <?php 
        include_once 'dbConnection.php';

        $result = mysqli_query($con, "SELECT * FROM user where role = 'teacher'") or die();
        


        while ($row = mysqli_fetch_array($result)) {
            $userID = $row['userID'];
            $phone = $row['phone'];
            $email = $row['email'];
            $address = $row['address'];
            $fullname = $row['name'];
            if($role != 'teacher'){
            echo '
                
                <tr>
                    <td>'.$userID.'</td>
                    <td>'.$fullname.'</td>
                    <td>'.$email.'</td>
                    <td>'.$phone.'</td>
                    <td>'.$address.'</td>';
                    if($name == 'admin'){
                        echo '<td class="mx-auto">
                                <form id="delete">
                                    <input type="text" style="display:none"  value="'.$userID.'"  id="remove">
                                    <button type="submit" class="btn btn-danger btn-flat " title="Remove" >
                                        <i class="fas fa-trash exam-icon"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                        ';
                    }
                    
            } else {
                echo '
                
                <tr>
                    <td>'.$userID.'</td>
                    <td>'.$fullname.'</td>
                    <td>'.$email.'</td>
                    <td>'.$phone.'</td>
                    <td>'.$address.'</td>
                </tr>
                ';
            }
        }
        ?>

                    </tbody>
                </table>
            </div>
        </div>
    </section>
    </div>

    <script>
    $("#delete").submit(function(e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: 'teacher/delete.php',
            data: {
                id: $("input#remove").val()
            },
            success: function(output) {
                alert(output)

                setTimeout(function() {
                    document.location.href = "dashboard.php?q=2"
                }, 500);

            },
            error: function(request, status, error) {
                console.log(error)
            }
        });


    });
    </script>