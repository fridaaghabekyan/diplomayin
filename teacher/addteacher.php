    <?php echo $role; ?>
    <section class="content">
      <br><br>
      <a href="dashboard.php?q=2" class="floatRTL btn btn-danger btn-flat pull-right marginBottom15"
          style="color:#fff">Cancel add</a>
      <div class="box col-xs-12">
          <div class="box-header">
              <h3 class="box-title">Add teacher</h3>
          </div>
          <div class="box-body table-responsive">
              <form class="form-horizontal" method="post" action="" name="addAdmin" id="teacherForm">
                  <div class="form-group form-row has-error">
                      <label for="inputEmail3" class="col-sm-2 control-label ">Full name * </label>
                      <div class="col-sm-10">
                          <input type="text" name="fullName" class="form-control" required="" placeholder="Full name">
                      </div>
                  </div>
                  <div class="form-group form-row has-error">
                      <label for="inputEmail3" class="col-sm-2 control-label ">Username * </label>
                      <div class="col-sm-10">
                          <input type="text" name="username" class="form-control" required="" placeholder="Username">
                      </div>
                  </div>
                  <div class="form-group form-row has-error">
                      <label for="inputEmail3" class="col-sm-2 control-label ">Address * </label>
                      <div class="col-sm-10">
                          <input type="text" name="address" class="form-control" required="" placeholder="Address">
                      </div>
                  </div>
                  <div class="form-group form-row has-error">
                      <label for="inputPassword3" class="col-sm-2 control-label ">Email address *</label>
                      <div class="col-sm-10">
                          <input type="email" name="email" class="form-control ng-valid-email"
                              placeholder="Email address" required="">
                      </div>
                  </div>
                  <div class="form-group form-row has-error">
                      <label for="inputPassword3" class="col-sm-2 control-label ">Password *</label>
                      <div class="col-sm-10">
                          <input type="password" name="password" class="form-control" required=""
                              placeholder="Password">
                      </div>
                  </div>
                  <div class="form-group form-row">
                      <label for="inputPassword3" class="col-sm-2 control-label ">Gender</label>
                      <div class="col-sm-10">
                          <div class="radio">
                              <label class="">
                                  <input type="radio" name="gender" value="male" class=" ng-valid">
                                  Male
                              </label>
                          </div>
                          <div class="radio">
                              <label class="">
                                  <input type="radio" name="gender" value="female" class=" ng-valid">
                                  Female
                              </label>
                          </div>
                      </div>
                  </div>
                  <div class="form-group form-row">
                      <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-default">Add teacher</button>
                      </div>
                  </div>
              </form>
          </div>
      </div>
  </section>
  </div>

  <script>
$("#teacherForm").submit(function(e) {
    e.preventDefault();
    $.ajax({
        type: "POST",
        url: 'teacher/adding.php',
        data: {
            name: $("input[name=fullName]").val(),
            username: $("input[name=username]").val(),
            email: $("input[name=email]").val(),
            password: $("input[name=password]").val(),
            address: $("input[name=address]").val(),
            gender: $("input[name=gender]:checked").val()
        },
        success: function(output) {
            alert(output)
            if (output == 'Username already existed') {
                $("input[name=username]").addClass('warning');
            } else {
                setTimeout(function() {
                    document.location.href = "dashboard.php?q=2"
                }, 500);
            }
        },
        error: function(request, status, error) {
            console.log(error)
        }
    });


});
  </script>