<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Online Examination System</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="css/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.4.0.min.js"
        integrity="sha256-BJeo0qm959uMBGb65z40ejJYGSgR7REI4+CW1fNKwOg=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
        integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <script src="js/main.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">

</head>

<body>
    <div class="app container-fluid">
        <div class="row">
            <div class="col-lg-2 col-md-3">
                <div id="sidenav">
                    <div class="wrapper">
                        <div class="logo">
                            <a href="#">Dashboard</a>
                            <a href="#" class="nav-icon pull-right"><i class="fa fa-bars"></i></a>
                        </div>
                        <div class="menu">
                            <ul>
                                <li class=" <?php if(@$_GET['q'] == 0){ echo 'active'; }?>"><a href="#">Home</a></li>
                                <!-- If login as admin -->
                                <li
                                    class="<?php if(@$_GET['q'] == 1 || @$_GET['q'] == 9 || @$_GET['q'] == 10 ){ echo 'active'; }?>">
                                    <a href="dashboard.php?q=1">My Account</a>
                                    <ul>
                                        <li><a href="dashboard.php?q=9">Settings</a></li>
                                        <li><a href="dashboard.php?q=10">Change Password</a></li>
                                    </ul>
                                </li>
                                <!-- End If login as admin -->
                                <li class="<?php if(@$_GET['q'] == 2 || @$_GET['q'] == 7){ echo 'active'; }?>"><a
                                        href="dashboard.php?q=2">Teachers</a></li>
                                <li class="<?php if(@$_GET['q'] == 3 || @$_GET['q'] == 8){ echo 'active'; }?>"><a
                                        href="dashboard.php?q=3">Students</a></li>
                                <li
                                    class="<?php if((@$_GET['q'] == 4) || (@$_GET['q'] == 5) ||(@$_GET['q'] == 6)){ echo 'active'; }?>">
                                    <a href="dashboard.php?q=4">Online Exams</a>
                                    <ul>
                                        <li><a href="dashboard.php?q=4">Exams List</a></li>
                                        <li><a href="dashboard.php?q=6">Results</a></li>
                                    </ul>
                                </li>
                                <li><a href="logout.php?q=index.php" class="logout">Log Out</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <main class="content col-lg-10 col-md-9">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <nav class="navbar navbar-expand-lg navbar-light ">
                                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                    <ul class="navbar-nav ml-auto">
                                        <li class="nav-item dropdown">
                                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
                                                role="button" data-toggle="dropdown" aria-haspopup="true"
                                                aria-expanded="false">
                                                <?php  echo $name;?>
                                            </a>
                                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                                <a class="dropdown-item" href="#">Change Password</a>
                                                <a class="dropdown-item" href="#">Account Settings</a>
                                            </div>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="logout.php?q=index.php">Logout</a>
                                        </li>
                                    </ul>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
                <?php

                if(@$_GET['q'] == 0){
                    if(($role != 'teacher') or ($role != 'student')){
                        include_once 'home.php';                 
                    } else {

                    }

                } else if(@$_GET['q'] == 1){
                    include_once 'account/account.php';
                } else if(@$_GET['q'] == 2){
                    include_once 'teacher/teachers.php';
                } else if(@$_GET['q'] == 3){
                    include_once 'student/students.php';
                } else if(@$_GET['q'] == 4){
                    include_once 'profiles/userProfile.php';
                } else if(@$_GET['q'] == 5){
                    include_once 'addexam.php';
                } else if(@$_GET['q'] == 6){
                    include_once 'result.php';
                } else if(@$_GET['q'] == 7){
                    include_once 'teacher/addteacher.php';
                } else if(@$_GET['q'] == 8){
                    include_once 'student/addstudent.php';
                } else if(@$_GET['q'] == 9){
                    include_once 'account/settings.php';
                } else if(@$_GET['q'] == 10){
                    include_once 'account/changepwd.php'; 
                } else if(@$_GET['q'] == 11){
                    include_once 'startexam.php';             
                } else if(@$_GET['q'] == 12){
                    include_once 'startexam.php';
                } else if(@$_GET['q'] == 13){
                    include_once 'startexam.php';
                }                   
                
            ?>


            </main>

        </div>
    </div>
</body>

</html>