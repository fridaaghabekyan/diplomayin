
<?php

include_once 'dbConnection.php';

$count_of_teacher = mysqli_fetch_array(mysqli_query($con,"SELECT COUNT(*) FROM user where role ='teacher'"))[0];
$count_of_student = mysqli_fetch_array(mysqli_query($con,"SELECT COUNT(*) FROM user where role ='student'"))[0];
?>

    <div class="row">
        <div class="col-lg-12">
            <h2>Dashboard</h2>
            <br>
        </div>
        <div class="col-lg-6 col-xs-12">

            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3>
                        <?php echo $count_of_student; ?>
                    </h3>
                    <p>
                        Students
                    </p>
                </div>
                <div class="icon">
                    <i class="fa fa-fw fa-user"></i>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-xs-12">

            <div class="small-box bg-green">
                <div class="inner">
                    <h3>
                        <?php echo $count_of_teacher; ?>
                    </h3>
                    <p>
                        Teachers
                    </p>
                </div>
                <div class="icon">
                    <i class="fa fa-fw fa-male"></i>
                </div>
            </div>
        </div>
        
    </div>
</div>