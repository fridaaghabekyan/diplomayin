function validateForm() {
    var a = document.forms["form"]["password"].value;
    var b = document.forms["form"]["cpassword"].value;
    if (a != b) {
        alert("Passwords must match.");
        return false;
    }
}

$(document).ready(function() {


    // when opening the sidebar
    $('.nav-icon').on('click', function() {
        // open sidebar
        $('.menu').toggleClass('open');
    });

    // if dismiss or overlay was clicked
    $('#dismiss').on('click', function() {
        $('#sidenav').removeClass('open');
    });
});