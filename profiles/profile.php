<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Online Examination System</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="css/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.4.0.min.js"
        integrity="sha256-BJeo0qm959uMBGb65z40ejJYGSgR7REI4+CW1fNKwOg=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
        integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">


    <script src="js/main.js"></script>

</head>

<body>

    <br>
    <section class="content ">
        <a href="dashboard.php?q=5"
            class="floatRTL btn btn-success btn-flat pull-right marginBottom15 ng-binding addExam">Add
            exam</a>
        <br>
        <hr>
        <div class="box col-xs-12">
            <div class="box-header">
                <h3 class="box-title ng-binding">List exams</h3>

            </div>
            <div class="box-body table-responsive">
                <table class="table table-hover">
                    <tbody>
                        <tr>
                            <th>ID</th>
                            <th>Exam Name</th>
                            <th>Exam Time Limit</th>
                            <th>Operations</th>
                        </tr>
                        <?php 
                            include_once 'dbConnection.php';
                            $result = mysqli_query($con, "SELECT * FROM exams") or die();
                            
                            while ($row = mysqli_fetch_array($result)) {
                                $examID = $row['examID'];
                                $examTitle = $row['title'];
                                $examTime = $row['examTime'];
                                $countofquestion = $row['countofquestion'];
                                echo '                 
                                    <tr>
                                        <td>'.$examID.'</td>
                                        <td>'.$examTitle.'</td>
                                        <td>'.$examTime.'</td>
                                        <td>
                                            <a  href="dashboard.php?q=11&id='.$examID.'&countofquestion='.$countofquestion.'" class="btn btn-info btn-flat " title="Edit" tooltip="">
                                                <input type="text" name="id" value="'.$examID.'" style="display:none">                                                 
                                                <input type="text" name="count" value="'.$countofquestion.'" style="display:none">                                                 
                                                <i class="fas fa-edit exam-icon"></i>
                                            </a>
                                            <a href="dashboard.php?q=13&id='.$examID.'&countofquestion='.$countofquestion.'" type="button" class="btn btn-danger btn-flat " title="Remove" tooltip="">
                                                <input type="text" name="id" value="'.$examID.'" style="display:none">                                                 
                                                <input type="text" name="count" value="'.$countofquestion.'" style="display:none">                                                 
                                                <i class="fas fa-trash exam-icon"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    ';
                                }

                            ?>

                    </tbody>
                </table>
            </div>
        </div>
    </section>
    </main>

    </div>
    </div>
</body>


<script>
// $("a.btn.btn-success.btn-flat").click(function(e) {

//     e.preventDefault();

//     var countofquestion = $("input[name=count]").val();
//     var id = $("input[name=id]").val();

//     $.ajax({
//         type: "POST",
//         url: 'update.php',
//         data: {
//             examTitle: $("input[name=examTitle]").val(),
//             id: id,
//             countofquestion: countofquestion,
//         },
//         success: function(output) {
//             alert(output)
//             // setTimeout(function() {
//             //     document.location.href = "dashboard.php?q=4"
//             // }, 500);
//         },
//         error: function(request, status, error) {
//             alert(request + " " + status + " " + error);
//         } 
//     });


// });
</script>
</html>