
<?php
    include_once 'dbConnection.php';

    $existedresult = mysqli_query($con,"SELECT * FROM user WHERE username = '$name'") or die('Error');
    while($row = mysqli_fetch_array($existedresult)) {
        $name = $row['name'];
        $username = $row['username'];
        $email = $row['email'];
        $gender = $row['gender'];
        $birthday = $row['birthdate'];
        $address = $row['address'];
        $phone = $row['phone'];
        $password = $row['password'];
        $id = $row['userID'];

        echo '<section class="content">
        <br><br>
        <div class="box col-xs-12">
            <div class="box-header">
                <h3 class="box-title">Update settings</h3>
            </div>
            <div class="box-body table-responsive">
                <form class="form-horizontal" method="post" action="" id="settingsForm">
                    <div class="form-group form-row has-error">
                        <label for="inputEmail3" class="col-sm-2 control-label ">Full name </label>
                        <div class="col-sm-10">
                            <input type="text" name="fullName" class="form-control" value="'.$name.'" required="" placeholder="Full name">
                            <input type="text" name="id" class="form-control" style="display:none" value="'.$id.'" required="" placeholder="Full name">
                        </div>
                    </div>
                    <div class="form-group form-row has-error">
                        <label for="inputEmail3" class="col-sm-2 control-label ">Username </label>
                        <div class="col-sm-10">
                            <input type="text" name="username" class="form-control" value="'.$username.'" required="" placeholder="Username">
                        </div>
                    </div>
                    <div class="form-group form-row has-error">
                        <label for="inputEmail3" class="col-sm-2 control-label ">Address </label>
                        <div class="col-sm-10">
                            <input type="text" name="address" class="form-control" value="'. $address.'" required="" placeholder="Address">
                        </div>
                    </div> 
                    <div class="form-group form-row has-error">
                        <label class="col-sm-2 control-label ">Email address</label>
                        <div class="col-sm-10">
                            <input type="email" name="email" class="form-control" placeholder="Email address" value="'. $email.'" required="">
                        </div>
                    </div>       
                    <div class="form-group form-row has-error">
                        <label class="col-sm-2 control-label ">Birthday</label>
                        <div class="col-sm-10">
                            <input type="date" name="birthday" class="form-control" placeholder="Birthday"
                                value="'. $birthday.'" required="">
                        </div>
                    </div>
                    <div class="form-group form-row has-error">
                        <label class="col-sm-2 control-label ">Phone</label>
                        <div class="col-sm-10">
                            <input type="text" name="phone" class="form-control" placeholder="Phone"
                                value="'. $phone.'" required="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-default">Update</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>';

    }
?>


<script>
    
$("#settingsForm").submit(function(e) {
    e.preventDefault();
    $.ajax({
        type: "POST",
        url: 'account/update.php',
        data: {
            name: $("input[name=fullName]").val(),
            username: $("input[name=username]").val(),
            address: $("input[name=address]").val(),
            email: $("input[name=email]").val(),
            birthday: $("input[name=birthday]").val(),
            phone: $("input[name=phone]").val(),
            id: $("input[name=id]").val(),
           
        },
        success: function(output) {
            alert(output)
            if(output == 'Settings updated'){
                alert('yee')
            } else {
                setTimeout(function() {
                    document.location.href = "dashboard.php?q=1"
                }, 500);
            }
           
        },
        error: function(request, status, error) {
            console.log(error)
        }
    });


});

</script>