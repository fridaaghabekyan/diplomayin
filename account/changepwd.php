<?php
    include_once 'dbConnection.php';

    $existedresult = mysqli_query($con,"SELECT * FROM user WHERE username = '$name'") or die('Error');
    while($row = mysqli_fetch_array($existedresult)) {
        $name = $row['name'];
        $username = $row['username'];
        $email = $row['email'];
        $gender = $row['gender'];
        $birthday = $row['birthdate'];
        $address = $row['address'];
        $phone = $row['phone'];
        $password = $row['password'];
        $id = $row['userID'];

        echo '<section class="content">
        <br><br>
        <div class="box col-xs-12">
            <div class="box-header">
                <h3 class="box-title">Change Password</h3>
            </div>
            <div class="box-body table-responsive">
                <form class="form-horizontal" method="post" action="account/change-password.php" id="settingsForm">
                <input type="hidden" name="username" value="'.$name.'">

                    <div class="form-group form-row has-error">
                        <label class="col-sm-2 control-label ">Current password</label>
                        <div class="col-sm-10">
                            <input type="password" name="currentpwd" class="form-control" placeholder="Current Password"  required="">
                        </div>
                    </div>
                    <div class="form-group form-row has-error">
                        <label class="col-sm-2 control-label ">New password</label>
                        <div class="col-sm-10">
                            <input type="password" name="newpwd" class="form-control" placeholder="New password"  required="">
                        </div>
                    </div>
                    <div class="form-group form-row has-error">
                        <label class="col-sm-2 control-label ">Confirm password</label>
                        <div class="col-sm-10">
                            <input type="password" name="confirmpwd" class="form-control" placeholder="Confirm Password"  required="">
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-default">Update</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>';

    }
?>