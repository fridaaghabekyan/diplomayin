<?php
    include_once 'dbConnection.php';
    session_start();

    if (!(isset($_SESSION['username']))) {
        header("location:index.php");            
    } else {
        $name  = $_SESSION['username'];  
        $role  = $_SESSION['role'];  

        if($role =='student') {
            include_once 'sidebar/sidebarUser.php';
        } else {
            include_once 'sidebar/sidebar.php';
        }
    }
    if(@$_GET['q']== 'addqns') {
        $countofquestion=@$_GET['n'];
        $examID=@$_GET['examID'];
        $countEasy=0;
        $i=0;
        $m=0;
        $h=0;
        for($j = 0; $j < $countofquestion; $j++){
            $hardly=$_POST['hardly'.$j];
       
            if($hardly=='easy'){
                if ($i>=0) {
                    $i++;
                         
                    $qid=uniqid();
                    $qns=$_POST['qns'.$j];
                    $q3=mysqli_query($con,"INSERT INTO easy_question VALUES  ('$examID','$qid','$qns' , '$i' )");
                    $optionaid=uniqid();
                    $optionbid=uniqid();
                    $optioncid=uniqid();
                    $optiondid=uniqid();
                    $a=$_POST[$j.'1'];
                    $b=$_POST[$j.'2'];
                    $c=$_POST[$j.'3'];
                    $d=$_POST[$j.'4'];
        
        
                    $sql="INSERT INTO `easy_options` (qID, option, optionID) VALUES
                    ('$qid','$a','$optionaid'),
                    ('$qid','$b','$optionbid'),
                    ('$qid','$c','$optioncid'),
                    ('$qid','$d','$optiondid')";
                    $result = $con -> query($sql);
                
                    $rightAnswerOption=$_POST['ans'.$j];
                    switch($rightAnswerOption){
                    case 'a':
                        $ansid=$optionaid;
                        break;
                    case 'b':
                        $ansid=$optionbid;
                        break;
                    case 'c':
                        $ansid=$optioncid;
                        break;
                    case 'd':
                        $ansid=$optiondid;
                        break;
                    default:
                        $ansid=$optionaid;
                    }
                    
                    
                    $qans=mysqli_query($con,"INSERT INTO rightanswereasy VALUES  ('$qid','$ansid')");
                }
            } 

            if($hardly=='medium'){
                if ($m>=0) {
                    $m++;
                         
                    $qid=uniqid();
                    $qns=$_POST['qns'.$j];
                    $q3=mysqli_query($con,"INSERT INTO medium_question VALUES  ('$examID','$qid','$qns' , '$m' )");
                    $optionaid=uniqid();
                    $optionbid=uniqid();
                    $optioncid=uniqid();
                    $optiondid=uniqid();
                    $a=$_POST[$j.'1'];
                    $b=$_POST[$j.'2'];
                    $c=$_POST[$j.'3'];
                    $d=$_POST[$j.'4'];
        
        
                    $sql="INSERT INTO `medium_options` (qID, option, optionID) VALUES
                    ('$qid','$a','$optionaid'),
                    ('$qid','$b','$optionbid'),
                    ('$qid','$c','$optioncid'),
                    ('$qid','$d','$optiondid')";
                    $result = $con -> query($sql);
                
                    $rightAnswerOption=$_POST['ans'.$j];
                    switch($rightAnswerOption){
                    case 'a':
                        $ansid=$optionaid;
                        break;
                    case 'b':
                        $ansid=$optionbid;
                        break;
                    case 'c':
                        $ansid=$optioncid;
                        break;
                    case 'd':
                        $ansid=$optiondid;
                        break;
                    default:
                        $ansid=$optionaid;
                    }                    
                    $qans=mysqli_query($con,"INSERT INTO right_answer_medium VALUES  ('$qid','$ansid')");
                }
                
            }

            if($hardly=='hard'){
                if ($h>=0) {
                    $h++;
                         
                    $qid=uniqid();
                    $qns=$_POST['qns'.$j];
                    $q3=mysqli_query($con,"INSERT INTO hard_question VALUES  ('$examID','$qid','$qns' , '$h' )");
                    $optionaid=uniqid();
                    $optionbid=uniqid();
                    $optioncid=uniqid();
                    $optiondid=uniqid();
                    $a=$_POST[$j.'1'];
                    $b=$_POST[$j.'2'];
                    $c=$_POST[$j.'3'];
                    $d=$_POST[$j.'4'];
        
        
                    $sql="INSERT INTO `hard_options` (qID, option, optionID) VALUES
                    ('$qid','$a','$optionaid'),
                    ('$qid','$b','$optionbid'),
                    ('$qid','$c','$optioncid'),
                    ('$qid','$d','$optiondid')";
                    $result = $con -> query($sql);
                
                    $rightAnswerOption=$_POST['ans'.$j];
                    switch($rightAnswerOption){
                    case 'a':
                        $ansid=$optionaid;
                        break;
                    case 'b':
                        $ansid=$optionbid;
                        break;
                    case 'c':
                        $ansid=$optioncid;
                        break;
                    case 'd':
                        $ansid=$optiondid;
                        break;
                    default:
                        $ansid=$optionaid;
                    }
                    
                    
                    $qans=mysqli_query($con,"INSERT INTO right_answer_hard VALUES  ('$qid','$ansid')");
                }              
            }
        }
       
    }
   



    // TRUNCATE `testfinalproject`.`hard_options`;
    // TRUNCATE `testfinalproject`.`medium_options`;
    // TRUNCATE `testfinalproject`.`easy_options`;
    // TRUNCATE `testfinalproject`.`hard_question`;
    // TRUNCATE `testfinalproject`.`easy_question`;
    // TRUNCATE `testfinalproject`.`medium_question`;
    // TRUNCATE `testfinalproject`.`exams`;
    // TRUNCATE `testfinalproject`.`rightanswereasy`;
    // TRUNCATE `testfinalproject`.`right_answer_medium`;
    // TRUNCATE `testfinalproject`.`right_answer_hard`;
?>